# CoAppExam

NodeJS implementation of a rest API for people db interface

## Requirements
1. MongoDB (either installed or using docker `docker run -d -p 27017:27017 --name mongo mongo`)
2. Node

## Setup
### For Development Setup
1. `npm install`
2. `npm run dev`

### For Production Setup
1. `npm install --only=production`
2. `npm start`