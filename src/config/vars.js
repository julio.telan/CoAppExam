const env = process.env.NODE_ENV || 'development';

module.exports = {
  env,
  port: process.env.PORT || 9292,
  mongo: {
    uri: 'mongodb://127.0.0.1:27017/people',
  },
  logs: env === 'production' ? 'combined' : 'dev'
};
