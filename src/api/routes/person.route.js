const express = require('express');
const validate = require('express-validation');
const controller = require('../controllers/person.controller');
const {
  listPeople,
  createPerson,
  getPerson,
  updatePerson,
  deletePerson
} = require('../validations/person.validation');

const router = express.Router();

/**
 * Load person when API with personId route parameter is hit
 */
router.param('personId', controller.load);


router
  .route('/')
  /**
   * @api {get} v1/person List People
   * @apiDescription Get a list of People
   * @apiVersion 1.0.0
   * @apiName ListPeople
   *
   * @apiParam  {Number{1-}}         [page=1]     List page
   * @apiParam  {Number{1-100}}      [perPage=1]  People per page
   *
   * @apiSuccess {Object[]} List of people.
   */
  .get(validate(listPeople), controller.list)
  /**
   * @api {post} v1/person Create a Person
   * @apiDescription Create a Person
   * @apiVersion 1.0.0
   * @apiName CreatePerson
   *
   * @apiParam  {String}        first_name       First name of the person
   * @apiParam  {String}        last_name        Last name of the person
   * @apiParam  {Number}        contact_number   Contact number of the person
   *
   */
  .post(validate(createPerson), controller.create);

router
  .route('/:personId')
  /**
   * @api {get} v1/person Get a specific person
   * @apiDescription Get a specific of People
   * @apiVersion 1.0.0
   * @apiName GetPerson
   *
   * @apiParam  {String}      personId  Id of the specific Person
   *
   * @apiSuccess {Object} Get a specific person.
   */
  .get(validate(getPerson), controller.get)
  /**
   * @api {put} v1/person Updates a person object
   * @apiDescription Updates a person object
   * @apiVersion 1.0.0
   * @apiName UpdatePerson
   *
   * @apiParam  {String}        first_name       First name of the person
   * @apiParam  {String}        last_name        Last name of the person
   * @apiParam  {Number}        contact_number   Contact number of the person
   *
   */
  .put(validate(updatePerson), controller.update)
  /**
   * @api {delete} v1/person Deletes a person object
   * @apiDescription Deletes a person object
   * @apiVersion 1.0.0
   * @apiName DeletePerson
   *
   * @apiParam  {String}      personId  Id of the specific Person
   */
  .delete(validate(deletePerson), controller.delete);

module.exports = router;
