const express = require('express');
const personRoutes = require('./person.route');

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));

router.use('/person', personRoutes);

module.exports = router;
