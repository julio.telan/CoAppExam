const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');

/**
 * Person Schema
 * @private
 */
const personSchema = new mongoose.Schema({
  first_name: {
    type: String,
    maxlength: 128,
    index: true,
    trim: true,
  },
  last_name: {
    type: String,
    maxlength: 128,
    index: true,
    trim: true,
  },
  contact_number: {
    type: String,
    index: true,
    trim: true,
  },
}, {
  timestamps: true,
});

/**
 * Static Methods
 */
personSchema.statics = {
  /**
   * Get person
   *
   * @param {ObjectId} id - The objectId of person.
   * @returns {Promise<Person, APIError>}
   */
  async get(id) {
    try {
      let person;

      if (mongoose.Types.ObjectId.isValid(id)) {
        person = await this.findById(id).exec();
      }
      if (person) {
        return person;
      }

      throw new APIError({
        message: 'Person does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },

  /**
   * List people
   *
   * @param {number} skip - Number of people to be skipped.
   * @param {number} limit - Limit number of people to be returned.
   * @returns {Promise<Person[]>}
   */
  async list({
    page = 1, perPage = 30
  }) {
    const people = await this.find()
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
    return {
      data: people,
      metadata: {
        page,
        perPage,
        count: people.length,
        totalCount: await this.countDocuments().exec()
      }
    };
  },
};

/**
 * @typedef Person
 */
module.exports = mongoose.model('Person', personSchema);
