const httpStatus = require('http-status');
const Person = require('../models/person.model');
const { handler: errorHandler } = require('../middleware/error');

/**
 * Load person and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const person = await Person.get(id);
    req.locals = { person };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

/**
 * Get person
 * @public
 */
exports.get = (req, res) => res.json(req.locals.person);

/**
 * Create a new person object
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const person = new Person(req.body);
    const savedPerson = await person.save();
    res.status(httpStatus.CREATED);
    res.json(savedPerson);
  } catch (error) {
    next(e);
  }
};

/**
 * Update existing person
 * @public
 */
exports.update = (req, res, next) => {
  const updatedPerson = req.body;
  const person = Object.assign(req.locals.person, updatedPerson);

  person.save()
    .then(savedPerson => res.json(savedPerson))
    .catch(e => next(e));
};

/**
 * Get people list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const people = await Person.list(req.query);
    res.json(people);
  } catch (error) {
    next(error);
  }
};

/**
 * Deletes a person
 * @public
 */
exports.delete = (req, res, next) => {
  const { person } = req.locals;

  person.remove()
    .then(() => res.status(httpStatus.NO_CONTENT).end())
    .catch(e => next(e));
};
