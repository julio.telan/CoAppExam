const Joi = require('joi');

module.exports = {

  // GET /v1/person
  listPeople: {
    query: {
      page: Joi.number().min(1),
      perPage: Joi.number().min(1).max(100),
    },
  },

  // GET /v1/person/:personId
  getPerson: {
    params: {
      personId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
    },
  },

  // POST /v1/person
  createPerson: {
    body: {
      first_name: Joi.string().max(128).required(),
      last_name: Joi.string().max(128).required(),
      contact_number: Joi.string().required(),
    },
  },

  // PUT /v1/person/:personId
  updatePerson: {
    body: {
      first_name: Joi.string().max(128).required(),
      last_name: Joi.string().max(128).required(),
      contact_number: Joi.string().required(),
    },
    params: {
      personId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
    },
  },

  // DELETE /v1/person/:personId
  deletePerson: {
    params: {
      personId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
    },
  },
};
